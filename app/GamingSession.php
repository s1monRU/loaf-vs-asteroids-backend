<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 2/15/2019
 * Time: 11:37 AM
 */

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

/**
 * Class GamingSession
 * Eloquent for a gaming session
 * @package App
 */
class GamingSession extends Eloquent
{
    /**
     * @var string Name of collection
     */
    protected $collection = 'gaming_sessions';

    /**
     * @var string Primary key
     */
    protected $primaryKey = '_id';

    /**
     * @var array Hidden fields
     */
    protected $hidden = ['_id'];

    /**
     * @return mixed Embedded eloquent
     */
    public function logs()
    {
        return $this->embedsMany('App\Log');
    }
}