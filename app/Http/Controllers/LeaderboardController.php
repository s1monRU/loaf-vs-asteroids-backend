<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 2/17/2019
 * Time: 1:35 PM
 */

namespace App\Http\Controllers;


use App\GamingSession;

/**
 * Class LeaderboardController
 * @package App\Http\Controllers
 */
class LeaderboardController
{
    /**
     * Get the best players for specified period
     * @param string $period The period of time
     * @return \Illuminate\Http\JsonResponse Best players list
     */
    public function index($period = 'month')
    {
        switch ($period) {
            case 'today':
                $time = '-1 day'; break;
            case 'month':
                $time = '-1 month'; break;
            case 'year':
                $time = '-1 year'; break;
            default:
                $time = '-20 years'; break;
        }
        $leaderboard = GamingSession::where(
            'created_at', '>=', new \DateTime($time)
        )->orderBy('score', 'desc')->take(10)->get();

        return response()->json($leaderboard);
    }
}