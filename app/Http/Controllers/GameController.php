<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 2/16/2019
 * Time: 1:17 AM
 */

namespace App\Http\Controllers;


use App\GamingSession;
use Illuminate\Http\Request;

/**
 * Class GameController
 * The main game controller
 * @package App\Http\Controllers
 * @author Ivan Semenov <developer.ivan.semenov@gmail.com>
 */
class GameController extends Controller
{
    /**
     * Create a new game and generate a unique token for it
     * @return \Illuminate\Http\JsonResponse Game token
     */
    public function createAndGenerateToken()
    {
        $game = new GamingSession;
        $game->score = 0;
        $game->save();
        return response()->json($game->_id);
    }

    /**
     * Save the game difficulty
     * @param Request $request
     */
    public function saveDifficulty(Request $request)
    {
        $game = GamingSession::find($request->token);
        $game->difficulty = $request->difficulty;
        $game->save();
    }

    /**
     * Save the score
     * @param Request $request
     */
    public function saveScore(Request $request)
    {
        $game = GamingSession::find($request->token);
        $game->score = $request->score;
        $game->save();
    }

    /**
     * Save the player's name
     * @param Request $request
     */
    public function saveName(Request $request)
    {
        $game = GamingSession::find($request->token);
        $game->name = $request->name;
        $game->save();
    }

    /**
     * Log each game event
     * @param Request $request
     */
    public function log(Request $request)
    {
        $logs = $request->except(['token']);
        GamingSession::find($request->token)->logs()->create($logs);
    }
}