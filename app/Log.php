<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 2/24/2019
 * Time: 12:04 AM
 */

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

/**
 * Class Log
 * Eloquent for log
 * @package App
 */
class Log extends Eloquent
{
    /**
     * @var array The fields allowed to store
     */
    protected $fillable = ['event', 'properties', 'score'];

    /**
     * @return mixed Belonged eloquent
     */
    public function gamingSession()
    {
        return $this->belongsTo('App\GamingSession');
    }
}